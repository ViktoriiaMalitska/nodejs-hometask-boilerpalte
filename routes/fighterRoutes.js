const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.post('/', createFighterValid, (req, res, next)=> {
    try {
        const data = FighterService.createFightner(req);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next)=> {
    try {
        const data = FighterService.searchAll();
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware)

router.get('/:id', (req, res, next)=> {
    try {
        const data = FighterService.search(req);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware)

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        const data = FighterService.updateOne(req);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const data = FighterService.deleteOne(req);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);
// TODO: Implement route controllers for fighter

module.exports = router;