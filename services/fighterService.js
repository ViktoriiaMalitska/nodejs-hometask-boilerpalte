// const {fightner} = require('../models/fighter');

const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    createFightner(date) {
        const newFightner = date.body;
        const fightner = {
            name: newFightner.name,
            power: newFightner.power,
            health: newFightner.health,
            defense: newFightner.defense
        }
        const item = FighterRepository.create(fightner);
        if(!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const id = search.params;
        const item = FighterRepository.getOne(id);
        if(!item) {
            return null;
        }
        return item;
    }

    searchAll() {
        const items = FighterRepository.getAll();
        if(!items) {
            return null;
        }
        return items
    }

    updateOne(req) {
        const id = req.params.id;
        const data = req.body;
        const items = FighterRepository.update(id, data);
        if(!items) {
            return null;
        }
        return items
    }

    deleteOne(data) {
        const id = data.params.id;
        const items = FighterRepository.delete(id);
        if(items.length === 0) {
            return null;
        }
        return items[0]
    }
}

module.exports = new FighterService();