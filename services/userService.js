const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  createUser(date) {
    const newUser = date.body;
    const user = {
      firstName: newUser.firstName,
      lastName: newUser.lastName,
      email: newUser.email,
      phoneNumber: newUser.phoneNumber,
      password: newUser.password,
    };

    const item = UserRepository.create(user);
    if (!item) {
      return null;
    }
    return item;
  }

  search(search) {
    const id = search.params;
    const item = UserRepository.getOne(id);
    if (!item) {
      return null;
    }
    return item;
  }

  searchAll() {
    const items = UserRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  updateOne(req) {
    const id = req.params.id;
    const data = req.body;
    const items = UserRepository.update(id, data);
    if (!items) {
      return null;
    }
    return items;
  }

  deleteOne(data) {
    const id = data.params.id;
    const items = UserRepository.delete(id);
    if (items.length === 0) {
      return null;
    }
    return items[0];
  }
}

module.exports = new UserService();
