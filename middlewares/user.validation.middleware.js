const { user } = require("../models/user");
// const { UserService } = require("../services/userService");
const {UserRepository} = require("../repositories/userRepository");
const createUserValid = (req, res, next) => {
  try {
    const helperArrBody = Object.keys(req.body);
    const lengthArrBody = helperArrBody.length;
    if (req.body) {

      let checkMail = {};
      let mailExist = null;
      let checkPhone = {};
      let phoneExist = null;

      checkMail.email = req.body.email;
      checkPhone.phoneNumber = req.body.phoneNumber;

      mailExist = UserRepository.getOne(checkMail);
      phoneExist = UserRepository.getOne(checkPhone);
      
      const { email, firstName, password, lastName, phoneNumber } = req.body;
      if (
        !mailExist &&
        !phoneExist &&
        lengthArrBody === 5 &&
        email &&
        email.slice(-10).localeCompare("@gmail.com") === 0 &&
        password &&
        password.length >= 3 &&
        firstName &&
        lastName &&
        phoneNumber &&
        phoneNumber.slice(0, 4).localeCompare("+380") === 0 &&
        phoneNumber.length === 13 &&
        !req.body.id
      ) {
        next();
      } else {
        res
          .status(400)
          .json({ error: true, message: "User entity to create is not valid" });
      }
    }
  } catch (err) {
    res.err = err;
  }
};

const updateUserValid = (req, res, next) => {
  try {
    const userExist = UserRepository.getOne(req.params);
    let mailExist = null;
    let checkMail = {};
    let phoneExist = null;
    let checkPhone = {};


    if(req.body.email) {
      checkMail.email = req.body.email;
      mailExist = UserRepository.getOne(checkMail);
    }

    if(req.body.phoneNumber) {
      checkPhone.phoneNumber = req.body.phoneNumber;
      phoneExist = UserRepository.getOne(checkPhone);
    }
    if (userExist && !phoneExist && !mailExist && req.body && !req.body.id && req.params.id) {
      let arrReqBody = [];
      for (key in req.body) {
        if (!user.hasOwnProperty(key)) {
          return res.status(400).json({
            error: true,
            message: "User entity to update is not valid",
          });
        }
        switch (key) {
          case "email":
            arrReqBody.push(
              req.body[key].slice(-10).localeCompare("@gmail.com") === 0
            );
            break;
          case "password":
            arrReqBody.push(req.body[key].length >= 3);
            break;
          case "phoneNumber":
            arrReqBody.push(
              req.body[key].slice(0, 4).localeCompare("+380") === 0 &&
                req.body[key].length === 13
            );
            break;
          default:
            "end";
        }
      }
      if (arrReqBody.includes(false)) {
        res
          .status(400)
          .json({ error: true, message: "User entity to update is not valid" });
      } else {
        next();
      }
    } else {
      res
        .status(400)
        .json({ error: true, message: "User entity to update is not valid" });
    }
  } catch (err) {
    res.err = err;
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
