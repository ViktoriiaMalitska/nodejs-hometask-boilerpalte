const { fighter } = require("../models/fighter");
const { FighterRepository } = require("../repositories/fighterRepository");
// const {FighterService} = require("../services/fighterService");
const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  try {
    const helperArrBody = Object.keys(req.body);
    const lengthArrBody = helperArrBody.length;
    if (req.body) {
      const { name, power, health, defense } = req.body;
      let checkName = {};
      let nameExist = null;
    
        if(req.body.name){
          checkName.name = req.body.name;
          nameExist = FighterRepository.getOne(checkName)
        }
      if (
        !nameExist &&
        lengthArrBody === 4 &&
        !req.body.id &&
        power && Number(power) && power > 0 && power < 100 &&
        name && !!name &&
        health && Number(health) && health > 0 && health <= 100 && 
        defense && Number(defense) && defense > 0 && defense <= 10
      ) {
        next();
      } else {
        res
          .status(400)
          .json({ error: true, message: "Fighter entity to create is not valid" });
      }
    }
  } catch (err) {
    res.err = err;
  }
};

const updateFighterValid = (req, res, next) => {
  try {

    // when we use FighterService then system crash
    const fighterExist = FighterRepository.getOne(req.params);
    let checkName = {};
    let nameExist = null;
    
    if(req.body.name){
      checkName.name = req.body.name;
      nameExist = FighterRepository.getOne(checkName);
    }
   
    if (fighterExist && !nameExist && req.body && !req.body.id && req.params.id) {
      let arrReqBody = [];
      for (key in req.body) {
        
        if (!fighter.hasOwnProperty(key)) {
          return res.status(400).json({
            error: true,
            message: "Fighter entity to update is not valid",
          });
        }
        switch (key) {
          case "power":
            arrReqBody.push(!!(Number(req.body[key])) && req.body[key] > 0 && req.body[key] < 100);
            break;
          case "defense":
            arrReqBody.push(!!(Number(req.body[key])) && (req.body[key] > 0) && (req.body[key] <= 10));
            break;
          case "health":
            arrReqBody.push(!!(Number(req.body[key])) && (req.body[key] > 0) && (req.body[key] <= 100));
            break;
          default:
            "end";
        }
      }
      if (arrReqBody.includes(false)) {
        res
          .status(400)
          .json({
            error: true,
            message: "Fighter entity to update is not valid",
          });
      } else {
        next();
      }
    } else {
      res
        .status(400)
        .json({
          error: true,
          message: "Fighter entity to update is not valid",
        });
    }
  } catch (err) {
    res.err = err;
  }
  // TODO: Implement validatior for fighter entity during update
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
