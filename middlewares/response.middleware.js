const responseMiddleware = (req, res, next) => {
 
  if (!res.data) {
    res.status(404).json({ error: true, message: "Not found" });
  } 
  else if(res.err)  {
    res.status(400).json({ error: true, message: "Processing problems" });
  }
  else if(res.data) {
    res.status(200).json(res.data);
  }
  // TODO: Implement middleware that returns result of the query
  next();
};

exports.responseMiddleware = responseMiddleware;
